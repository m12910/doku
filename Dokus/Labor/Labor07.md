
# Dokumentation Labor 7 - Erweiterung Labor 6 mit Firewall

 - Datum: 04.02.2022
 - Name: Tim Schefer
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](img/Labor07.PNG)


## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/ip pool
add name=dhcp_pool1 ranges=192.168.0.10-192.168.0.254
/ip dhcp-server
add address-pool=dhcp_pool1 interface=ether1 name=dhcp1

/ip address
add address=192.168.0.1/24 interface=ether1 network=192.168.0.0
add address=192.168.23.20/24 interface=ether2 network=192.168.23.0
add address=192.168.255.2/30 interface=ether3 network=192.168.255.0

/ip dhcp-client
add interface=ether1
/ip dhcp-server network
add address=192.168.0.0/24 dns-server=8.8.8.8 gateway=192.168.0.1

/ip firewall nat
add action=masquerade chain=srcnat out-interface=ether3
add action=masquerade chain=srcnat out-interface=ether3

/ip route
add dst-address=0.0.0.0/0 gateway=192.168.255.1
add dst-address=0.0.0.0/0 gateway=192.168.255.1

/system identity
set name=R1

```

## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/ip address
add address=192.168.23.21/24 interface=ether1 network=192.168.23.0
add address=192.168.255.1/30 interface=ether3 network=192.168.255.0
add address=192.168.255.5/30 interface=ether4 network=192.168.255.4

/ip dhcp-client
add interface=ether1
add interface=ether2

/ip firewall nat
add action=masquerade chain=srcnat out-interface=ether2

/system identity
set name=R2

```

## Config R3
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/ip pool
add name=dhcp_pool1 ranges=192.168.0.10-192.168.0.254
/ip dhcp-server
add address-pool=dhcp_pool1 interface=ether1 name=dhcp1

/ip address
add address=192.168.0.1/24 interface=ether1 network=192.168.0.0
add address=192.168.23.22/24 interface=ether3 network=192.168.23.0
add address=192.168.255.6/30 interface=ether4 network=192.168.255.4

/ip dhcp-client
add interface=ether1
/ip dhcp-server network
add address=192.168.0.0/24 dns-server=8.8.8.8 gateway=192.168.0.1

/ip firewall nat
add action=masquerade chain=srcnat out-interface=ether4

/ip route
add dst-address=0.0.0.0/0 gateway=192.168.255.5

/system identity
set name=R3

```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```

## Quellen
 - https://github.com/ReddixT/M129/tree/master/A7#readme
 

## Reflexion
Da ich zuvor schon Labor 6 gelöst habe war in Labor 7 nichts mehr neu, und ich konnte es ohne grössere schwierigkeiten lösen. Es war mich ausserdem möglich meine Fähigkeiten zu vertiefen, und ich
denke dass ich in diesem Modul auch für die Zukunft viel profitieren konnte. 