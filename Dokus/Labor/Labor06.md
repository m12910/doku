
# Dokumentation Labor 6 - Labor mit zwei Router (NAT ins Internet), Debian Client

 - Datum: 03.02.2022
 - Name: Tim Schefer
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](img/Labor06.PNG)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address/ add address=192.168.255.1/30 interface=ether3

/ip/route/ add dst-address=192.168.50.0/24 gateway=192.168.255.2

/ip/dhcp-client/ add interface=ether1
/ip/dhcp-client/ add interface=ether2

/ip/firewall/nat/ add action=masquerade chain=srcnat out-interface=ether2
```

## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R2
/ip/address/ add address=192.168.50.1/24 interface=ether1
/ip/address/ add address=192.168.23.141/24 interface=ether2
/ip/address/ add address=192.168.255.2/30 interface=ether3

/ip/route/ add dst-address=0.0.0.0/0 gateway=192.168.255.1

/ip/pool/ add name=dhcp_pool1 ranges=192.168.50.10-192.168.50.254
/ip/dhcp-server/ add address-pool=dhcp_pool1 interface=ether1 name=dhcp1
/ip/dhcp-client/ add interface=ether1
/ip/dhcp-server/network/ add address=192.168.50.0/24 gateway=192.168.50.1
```

## Config PC1
 - [Debian 10 Minimal](https://mikrotik.com/download)
 - 1 Interface Enabled
 - Credentials: debian
```
su
vi /etc/network/interfaces
	auto ens3
	iface ens3 inet dhcp

apt update && apt upgrade -y
apt install lxde-core
apt clean
apt install xinit
startx

apt install chromium
```

## Quellen
 - https://wiki.mikrotik.com/wiki/Manual:IP/Firewall/NAT
 - https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Server
 - https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Client
 - https://wiki.debian.org/de/DHCP_Client

## Neue Lerninhalte
 - NAT einrichten
 - dhcp unter linux konfigurieren

## Reflexion
Das NAT einzurichten war einfacher als ich gedacht habe, da MikroTik zum Glück alles sehr gut dokumentiert hat. Mir hat es besonders spass gemacht den debian client zu konfigurieren,
auch wenn man dank TBZ Wlan manchmal etwas länger warten musste um Pakete herunterzuladen.
