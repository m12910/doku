
# Dokumentation Labor2 - Ping mit Router

 - Datum: 09.01.2022
 - Name: Tim Schefer
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](img/Labor02.PNG) 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 2 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.1.1/24 interface=ether1
/ip/address add address=192.168.2.1/24 interface=ether2
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.1.2 255.255.255.0 192.168.1.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [GATEWAY]
ip 192.168.2.2 255.255.255.0 192.168.2.1
```


## Quellen
 - https://youtube.com/watch?v=Ez2rVINjj2g
 - https://wiki.mikrotik.com/wiki/Manual:IP/Address#Example
 - Compendio

## Neue Lerninhalte
 - MikroTik RouterOS war für mich komplett neue (Keine Erfahrungen)
 - Mehrere Interfaces auf Router konfigurieren
 - Erfahrungen zur Bedienung von GNS3 habe ich bereits in der ersten Übung gesammelt. Neu war das ganze Routing 

## Reflexion
Am Anfang hatte ich noch ziemlich probleme weil ich nicht verstand wie Routing grundsätzlich funktioniert. Nachdem ich mir das Compendio durchgelesen,
und ein paar Youtube Videos zu dem Thema geschaut habe wurde mir alles klar, und es war nurnoch eine frage der Konfiguration auf dem Router.