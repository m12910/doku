
# Dokumentation Übung - Mein PC pingt ins Labor

 - Datum: 26.11.2021
 - Name: Tim Schefer
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/20_GNS3%20Einf%C3%BChrung)

![GNS3 Screenshot meines Labors](img/2_VPCs.png)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
# [IP] [MASKE]
ip 192.168.1.2 255.255.255.0
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
# [IP] [MASKE]
ip 192.168.1.3 255.255.255.0
```

## Config Switch1
- [GNS3 Ethernet Switch](https://docs.gn3.com/search?q=switch)
- 2x Ports belegt

## Quellen
 -  https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/20_GNS3%20Einf%C3%BChrung

## Neue Lerninhalte
 - GNS3 Einrichten und Kennenlernen, war für mich bis jetzt komplett neu
 - Dokumentation mit Markdown erstellen, ebenfalls noch nie gemacht
 - VPCs mit IP Adresse und Subnetzmaske Konfigurieren und in betrieb nehmen

## Reflexion
Am anfang war sehr vieles neu für mich da ich noch nie mit GNS3 oder Markdown gearbeitet habe. Ich finde allerdings das beide Themen
sehr spannend sind. Da im [Gitlab](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/) alles durch kurze Videos Dokumentiert ist hatte ich keine Mühe dem Inhalt zu folgen.
