
# Dokumentation Labor 4 - Aggregierte statische Routen

 - Datum: 27.01.2021
 - Name: Tim Schefer
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](img/Labor04.PNG)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.28.1/24 interface=ether2
/ip/address add address=192.168.255.1/30 interface=ether3

/ip/route/ add dst-address=192.168.128.0/17 gateway=192.168.255.2
```

## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R2
/ip/address add address=192.168.255.2/30 interface=ether3
/ip/address add address=192.168.146.1/24 interface=ether2
/ip/address add address=192.168.255.5/30 interface=ether4

/ip/route/ add dst-address=192.168.210.0/24 gateway=192.168.255.6
/ip/route/ add dst-address=192.168.23.0/24 gateway=192.168.255.1
/ip/route/ add dst-address=192.168.28.0/24 gateway=192.168.255.1
```

## Config R3
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R3
/ip/address add address=192.168.255.6/30 interface=ether4
/ip/address add address=192.168.210.1/24 interface=ether2

/ip/route/ add dst-address=192.168.0.0/16 gateway=192.168.255.5
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.28.2 255.255.255.0 192.168.28.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [GATEWAY]
ip 192.168.146.2 255.255.255.0 192.168.146.1
```

## Config VPC 3
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC3
# [IP] [MASKE] [GATEWAY]
ip 192.168.210.2 255.255.255.0 192.168.210.1
```

## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.0.0 mask 255.255.0.0 192.168.23.137
```

## Quellen
 - https://www.youtube.com/watch?v=QqEcCzhlWis
 - https://www.youtube.com/watch?v=N54mZ5DDqYw

## Neue Lerninhalte
 - Aggregierte Routen

## Reflexion
Zuerst konnte ich mir nicht ganz vorstellen wie man das mit nur einer Route auf R1 lösen kann, aber zum Glück ist mir der Titel der aufgabe ins Auge gestochen, und als ich diesen auf Youtube
gesucht habe wurde ich fündig. Die Dokumentation von [René Ottenburg](https://github.com/ReddixT/M129) hat mir am schluss auch noch geholfen alles besser nachvollziehen zu können, auch wenn er im gegensatz zu mir mit Cisco arbeitet.