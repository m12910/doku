
# Dokumentation Übung - Mein PC pingt ins Labor

 - Datum: 03.12.2021
 - Name: Tim Schefer
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/20_GNS3%20Einf%C3%BChrung)

![GNS3 Screenshot meines Labors](img/01_PC-ins-Labor.png)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden. 

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 8 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.24.1/24 interface=ether2
/ip/address add address=192.168.23.16/24 interface=ether1
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.24.2 255.255.255.0 192.168.24.1
```

## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.24.0 mask 255.255.255.0 192.168.23.16
```

## Quellen
 - https://www.howtogeek.com/howto/windows/adding-a-tcpip-route-to-the-windows-routing-table/
 - https://wiki.mikrotik.com/wiki/Manual:IP/Address#Example
 - [Video](https://gitlab.com/ch-tbz-it/Stud/m129/-/raw/main/20_GNS3%20Einf%C3%BChrung/videos/GNS3_Mit_meinem_PC_ins_Labor_pingen.webm)

## Neue Lerninhalte
 - Lokale Route auf auf einem Windows 10 PC konfigurieren
 - MikroTik RouterOS war für mich komplett neu (Keine Erfahrungen)
 - IPv4-Adressen auf RouterOS konfigurieren
 - Erfahrungen zur Bedienung von GNS3 habe ich bereits in der ersten Übung gesammelt. Neu war die "Edit config" funktion, aber vor allem auch das CLI auf dem Router. 

## Reflexion
Dank der Übung finde ich mich mittlerweile schon etwas besser in GNS3 zurecht. Ich konnte mit dem Video zwar alles erfolgreich konfigurieren, allerdings habe ich noch nicht
zu 100% verstanden wie die einzelnen schritte funktionieren, und bin mehr oder weniger blind der Anleitung gefolgt.
Nächste Woche sollte allerdings noch ein Input zum Thema Routing folgen, dann wird das ganze hoffentlich schon etwas klarer für mich.
