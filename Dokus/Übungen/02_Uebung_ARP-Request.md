
# Dokumentation Übung - ARP Broadcast

 - Datum: 10.12.2021
 - Name: Tim Schefer
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/40_Wireshark)

![GNS3 Screenshot meines Labors](img/03_ARP.png) 


## Config Router
 - [MikroTik CHR 6.49.1](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
# add bridge0
/interface bridge add name=bridge0
# attach ports ether3 up to ether4 to bridge0
/interface bridge port add bridge=bridge0 interface=ether2
/interface bridge port add bridge=bridge0 interface=ether3
/interface bridge port add bridge=bridge0 interface=ether4
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE]
ip 192.168.2.101 255.255.255.0
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
# [IP] [MASKE]
ip 192.168.2.102 255.255.255.0
```

## Config VPC 3
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
# [IP] [MASKE]
ip 192.168.2.103 255.255.255.0
```

## Fragen
1. Wie viele ARP Pakete werden insgesamt übertragen?
- In unserem Fall sind es 2 Pakete, ein Broadcast und eine 2 Pakete, ein Broadcast und eine Antwort 
2. Wie lautet die MAC Adresse von PC1?
- 00:50:79:66:68:00
3. Wie lautet die MAC Adresse von PC2?
- 00:50:79:66:68:01
4. Wie viele Nutzdaten (Bytes) werde in einem *ICMP Echo-Request* übertragen?
- 56 Bytes
5. Was für (Menschen) lesbare Zeichen sind in den Nutzdaten in einem *ICMP Echo-Request* erhalten?
- 
6. Wie gross ist ein *ICMP Echo-Request* Frame insgesamt?
- 98 Bytes 
7. Sind die verwendeten MAC-Adressen *Globally Unique*?
- Nein sind sie nicht
8. Wie lautet der Name und der HEX-Wert des in Ethernet *encapsulated protocol* im ICMP Paket?
- 
9. Welcher "OSI-Layer" kommt in den *ARP-Frames* im Vergleich zu den *ICMP-Paketen* nicht vor?
- ARP Frames arbeiten nur auf Layer 2, während ICMP auch Layer 3 verwendet
10. Welcher OP-Code hat der *ARP-Reply*?
- reply (2)


## Quellen
 - Videos von Herr Albrecht
 - In Schule besprochenes Material

## Neue Lerninhalte
 - Wireshark kentnisse auffrischen
 - ARP & ICMP Protokoll besser kennenlernen
 - GNS 3 Kentnisse vertiefen

## Reflexion
Ich konnte zwar im ersten Lehrjahr bereits etwas erfahrung mit dem Programm Wireshark sammeln, allerdings ist vieles wieder
ein wenig vergessen gegangen, da ich es lange nicht mehr benutzt habe, und ich konnte mein wissen somit wieder etwas auffrischen.
Ausserdem war ich mir nicht mehr ganz sicher wie das ARP Protokoll funktioniert, aber dank dem auftrag habe ich mir auch das wieder
ins Gedächtnis rufen können.
